FROM maven:3.6-jdk-8-slim

RUN mkdir /var/maven/ && chmod -R 777 /var/maven
RUN mkdir mkdir /tmp/coref/ && chmod -R 777 /tmp/coref
ENV MAVEN_CONFIG /var/maven/.m2

ADD pom.xml /tmp/coref
ADD lib/nif* /tmp/coref/lib
RUN cd /tmp/coref && mvn -B -e -C -T 1C -Duser.home=/var/maven org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline 

RUN chmod -R 777 /var/maven
EXPOSE 8091 

COPY . /tmp/coref
WORKDIR /tmp/coref
RUN mvn -Duser.home=/var/maven clean install -DskipTests

RUN chmod -R 777 /tmp/coref

CMD mvn -Duser.home=/var/maven spring-boot:run
#CMD mvn -Duser.home=/var/maven -Drun.jvmArguments="-Xmx6144m" spring-boot:run
#CMD ["/bin/bash"]
