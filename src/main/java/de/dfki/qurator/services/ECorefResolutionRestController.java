package de.dfki.qurator.services;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.dfki.qurator.nif.ontologies.QURATORNIF;
import de.dfki.qurator.services.ecorefresolution.ECorefResolutionService;

@RestController
@RequestMapping("/eCoref")
public class ECorefResolutionRestController {
	
	Logger logger = Logger.getLogger(ECorefResolutionRestController.class);

	@Autowired
	ECorefResolutionService service;

	@RequestMapping(value = "/testURL", method = { RequestMethod.POST, RequestMethod.GET })
	public ResponseEntity<String> testURL(
			@RequestParam(value = "preffix", required = false) String preffix,
            @RequestBody(required = false) String postBody) throws Exception {
    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.add("Content-Type", "text/plain");
    	ResponseEntity<String> response = new ResponseEntity<String>("The restcontroller [eCorefResolution] is working properly", responseHeaders, HttpStatus.OK);
    	return response;
	}
	
	@RequestMapping(value = "/analyzeText", method = {RequestMethod.POST})
	public ResponseEntity<String> analyzeText(
			HttpServletRequest request,
			@RequestParam(value = "analysis", required = false) String analysis, // either ner or dict or temp or tfidf
			@RequestParam(value = "mode", required = false) String mode,
			@RequestParam(value = "language", required = false) String language,
			@RequestParam(value = "prefix", required = false) String prefix,
			@RequestParam(value = "content", required = true) boolean isContent,
			@RequestParam(value = "outputCallback", required = false) String outputCallback,
			@RequestParam(value = "statusCallback", required = false) String statusCallback,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		
		if (analysis == null || analysis.equalsIgnoreCase("")) {
			logger.error("Error: 'analysis' input parameter can not be null or empty. The folowing values are supported: 'language', 'dictionary' and 'rules'.");
			throw new Exception("Error: 'analysis' input parameter can not be null or empty. The folowing values are supported: 'language', 'dictionary' and 'rules'.");
		}
		if( !(analysis.equalsIgnoreCase("language") || analysis.equalsIgnoreCase("dictionary") || analysis.equalsIgnoreCase("rules")) ) {
			logger.error("Error: 'analysis' values is not supported. Only the folowing values are supported: 'language', 'dictionary' and 'rules'.");
			throw new Exception("Error: 'analysis' values is not supported. Only the folowing values are supported: 'language', 'dictionary' and 'rules'.");
		}
		if (language == null || language.equalsIgnoreCase("")) {
			language = "en";
			logger.warn("The input language is not defined. English (\"EN\") is used.");
		}
		if (postBody == null || postBody.equalsIgnoreCase("")) {
			logger.error("Error: body can not be null or empty. It must contain data for processing or document URL.");
			throw new Exception("Error: body can not be null or empty. It must contain data for processing or document URL.");
		}
		if (contentTypeHeader == null || contentTypeHeader.equalsIgnoreCase("")) {
			contentTypeHeader = "text/plain";
			logger.warn("The Content-Type is not defined. 'text/plain' is used.");
		}
		if (acceptHeader == null || acceptHeader.equalsIgnoreCase("")) {
			acceptHeader = "text/turtle";
			logger.warn("The Accept Header is not defined. 'text/turtle' is used.");
		}
        if (prefix == null || prefix.equalsIgnoreCase("")){
			prefix = QURATORNIF.getDefaultPrefix();
			logger.warn("The prefix is not defined. '"+QURATORNIF.getDefaultPrefix()+"' is used.");
		}
       	try {
            String textForProcessing = postBody;
			String documentURI = textForProcessing;
			String tripleStoreURL = "";
			String tripleStoreName = "";
            HttpHeaders responseHeaders = new HttpHeaders();
			if(outputCallback==null || outputCallback.equalsIgnoreCase("")) {
	    		String result = service.analyzeSynchronous(textForProcessing, contentTypeHeader, acceptHeader, prefix, mode, language, isContent, outputCallback);
	    		if(isContent) {
	                responseHeaders.add("Content-Type", acceptHeader);
	    		}
	    		else {
	    			//TODO Store the resulting information in the triple store.
//	    			NIFAdquirer.saveNIFDocumentInLKGManager(result, acceptHeader);
	    			result = "ERROR: getting NIF from external sources (such as triple store) is still not supported!!";
	                responseHeaders.add("Content-Type", "text/plain");
	    		}
	    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
	            return response;
			}
			else {
	    		String result = service.analyzeAsynchronous(textForProcessing, contentTypeHeader, acceptHeader, prefix, mode, language, isContent, outputCallback);
                responseHeaders.add("Content-Type", acceptHeader);
	    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.ACCEPTED);
	            return response;
			}
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/checkStatus", method = {RequestMethod.GET})
	public ResponseEntity<String> checkStatus(
			HttpServletRequest request,
			@RequestParam(value = "analysisId", required = false) String analysisId,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		
		if (analysisId == null || analysisId.equalsIgnoreCase("")) {
			logger.error("Error: 'analysisId' input parameter can not be null or empty. The folowing values are supported: 'language', 'dictionary' and 'rules'.");
			throw new Exception("Error: 'analysisId' input parameter can not be null or empty. The folowing values are supported: 'language', 'dictionary' and 'rules'.");
		}
       	try {
        	String result = service.checkStatus(analysisId);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Content-Type", "text/plain");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

	@RequestMapping(value = "/getOutput", method = {RequestMethod.GET})
	public ResponseEntity<String> getOutput(
			HttpServletRequest request,
			@RequestParam(value = "analysisId", required = false) String analysisId,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
            @RequestBody(required = false) String postBody) throws Exception {
		
		if (analysisId == null || analysisId.equalsIgnoreCase("")) {
			logger.error("Error: 'analysisId' input parameter can not be null or empty.");
			throw new Exception("Error: 'analysisId' input parameter can not be null or empty.");
		}
       	try {
        	String result = service.getOutput(analysisId,contentTypeHeader);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Content-Type", "text/plain");
    		ResponseEntity<String> response = new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
            return response;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
        }
    }

}
