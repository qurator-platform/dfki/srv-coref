package de.dfki.qurator.services.ecorefresolution.modules;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;

import de.dfki.qurator.nif.NifDocument;
import de.dfki.qurator.nif.ontologies.ITSRDF;
import de.dfki.qurator.nif.ontologies.NIF;
import de.dfki.qurator.nif.ontologies.RDFConstants.RDFSerialization;
import de.dfki.qurator.nif.processing.NIFConversionService;
import de.dfki.qurator.nif.processing.NIFReader;
import de.dfki.qurator.nif.processing.NIFWriter;
//import edu.stanford.nlp.hcoref.CorefCoreAnnotations;
//import edu.stanford.nlp.hcoref.data.CorefChain;
//import edu.stanford.nlp.hcoref.data.CorefChain.CorefMention;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.IntPair;
import edu.stanford.nlp.util.TypesafeMap;
import edu.stanford.nlp.coref.CorefCoreAnnotations;
import edu.stanford.nlp.coref.data.CorefChain;
import edu.stanford.nlp.coref.data.CorefChain.CorefMention;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.ie.util.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.semgraph.*;
import edu.stanford.nlp.trees.*;
import java.util.*;


public class Corefinizer {
	 public static void main(String[] args) throws Exception {
		 
		 String text = "Joe Smith was born in California. " +
			      "In 2017, he went to Paris, France in the summer. " +
			      "His flight left at 3:00pm on July 10th, 2017. " +
			      "After eating some escargot for the first time, Joe said, \"That was delicious!\" " +
			      "He sent a postcard to his sister Jane Smith. " +
			      "After hearing about Joe's trip, Jane decided she might go to France one day.";
//		 Properties props = new Properties();
//		    // set the list of annotators to run
//		    props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,coref");
//		    // set a property for an annotator, in this case the coref annotator is being set to use the neural algorithm
//		    props.setProperty("coref.algorithm", "neural");
//		    // get document wide coref info
//		    CoreDocument document = new CoreDocument(text);
//		    // annnotate the document
//		    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
//		    pipeline.annotate(document);
//		    Map<Integer, CorefChain> corefChains = document.corefChains();
//		    System.out.println("Example: coref chains for document");
//		    System.out.println(corefChains);
//		    System.out.println();
		 
		 Model nifModel; 
//		 NifDocument d = new NifDocument();
//		 nifModel = d.unserializeRDF(text, "text/plain").getModel();
		  
		
			String nifString = 
					"@prefix dktnif: <http://dkt.dfki.de/ontologies/nif#> .\n" +
							"@prefix dbo:   <http://dbpedia.org/ontology/> .\n" +
							"@prefix geo:   <http://www.w3.org/2003/01/geo/wgs84_pos/> .\n" +
							"@prefix nif-ann: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-annotation#> .\n" +
							"@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
							"@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n" +
							"@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n" +
							"@prefix itsrdf: <http://www.w3.org/2005/11/its/rdf#> .\n" +
							"@prefix nif:   <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .\n" +
							"@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=70,74>\n" +
							"        a                        nif:String , nif:RFC5147String ;\n" +
							"        nif:anchorOf             \"born\"^^xsd:string ;\n" +
							"        nif:beginIndex           \"70\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex             \"74\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:referenceContext     <http://dkt.dfki.de/documents/#char=0,86> ;\n" +
							"        nif:relationAction       \"bear\"^^xsd:string ;\n" +
							"        nif:relationObject       \"Hamburg(http://dkt.dfki.de/documents/#char=47,54)\"^^xsd:string ;\n" +
							"        nif:relationSubject      \"She(http://dkt.dfki.de/documents/#char=0,13)\"^^xsd:string ;\n" +
							"        nif:thematicRoleSubject  \"themRoleObj\"^^xsd:string ;\n" +
							"        itsrdf:taIdentRef        <http://dkt.dfki.de/entities/relation> .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=0,13>\n" +
							"        a                     nif:RFC5147String , nif:String ;\n" +
							"        dbo:birthDate         \"1954-07-17\"^^xsd:date ;\n" +
							"        nif:anchorOf          \"Angela Merkel\"^^xsd:string ;\n" +
							"        nif:beginIndex        \"0\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex          \"13\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:referenceContext  <http://dkt.dfki.de/documents/#char=0,86> ;\n" +
							"        itsrdf:taClassRef     dbo:Person ;\n" +
							"        itsrdf:taIdentRef     <http://dbpedia.org/resource/Angela_Merkel> .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=21,29>\n" +
							"        a                     nif:String , nif:RFC5147String ;\n" +
							"        nif:anchorOf          \"an alien\"^^xsd:string ;\n" +
							"        nif:beginIndex        \"21\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex          \"29\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:referenceContext  <http://dkt.dfki.de/documents/#char=0,86> ;\n" +
							"        owl:sameAs            \"http://dkt.dfki.de/documents/#char=0,13\"^^xsd:string .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=56,59>\n" +
							"        a                     nif:String , nif:RFC5147String ;\n" +
							"        nif:anchorOf          \"She\"^^xsd:string ;\n" +
							"        nif:beginIndex        \"56\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex          \"59\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:referenceContext  <http://dkt.dfki.de/documents/#char=0,86> ;\n" +
							"        owl:sameAs            \"http://dkt.dfki.de/documents/#char=0,13\"^^xsd:string .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=0,86>\n" +
							"        a                        nif:RFC5147String , nif:String , nif:Context ;\n" +
							"        dktnif:averageLatitude   \"52.96361111111111\"^^xsd:double ;\n" +
							"        dktnif:averageLongitude  \"11.504722222222222\"^^xsd:double ;\n" +
							"        dktnif:standardDeviationLatitude\n" +
							"                \"0.6016666666666666\"^^xsd:double ;\n" +
							"        dktnif:standardDeviationLongitude\n" +
							"                \"1.5033333333333339\"^^xsd:double ;\n" +
							"        nif:beginIndex           \"0\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex             \"86\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:isString             \"Angela Merkel is not an alien. She was born in Hamburg. She then moved to Brandenburg.\"^^xsd:string .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=14,16>\n" +
							"        a                        nif:RFC5147String , nif:String ;\n" +
							"        nif:anchorOf             \"is\"^^xsd:string ;\n" +
							"        nif:beginIndex           \"14\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex             \"16\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:referenceContext     <http://dkt.dfki.de/documents/#char=0,86> ;\n" +
							"        nif:relationAction       \"be\"^^xsd:string ;\n" +
							"        nif:relationObject       \"alien(null)\"^^xsd:string ;\n" +
							"        nif:relationSubject      \"Merkel(http://dkt.dfki.de/documents/#char=0,13)\"^^xsd:string ;\n" +
							"        nif:thematicRoleSubject  \"themRoleObj\"^^xsd:string ;\n" +
							"        itsrdf:taIdentRef        <http://dkt.dfki.de/entities/relation> .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=47,54>\n" +
							"        a                     nif:String , nif:RFC5147String ;\n" +
							"        nif:anchorOf          \"Hamburg\"^^xsd:string ;\n" +
							"        nif:beginIndex        \"47\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex          \"54\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:referenceContext  <http://dkt.dfki.de/documents/#char=0,86> ;\n" +
							"        geo:lat               \"53.56527777777778\"^^xsd:double ;\n" +
							"        geo:long              \"10.001388888888888\"^^xsd:double ;\n" +
							"        itsrdf:taClassRef     dbo:Location ;\n" +
							"        itsrdf:taIdentRef     <http://dbpedia.org/resource/Hamburg> .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=74,85>\n" +
							"        a                     nif:RFC5147String , nif:String ;\n" +
							"        nif:anchorOf          \"Brandenburg\"^^xsd:string ;\n" +
							"        nif:beginIndex        \"74\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex          \"85\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:referenceContext  <http://dkt.dfki.de/documents/#char=0,86> ;\n" +
							"        geo:lat               \"52.36194444444445\"^^xsd:double ;\n" +
							"        geo:long              \"13.008055555555556\"^^xsd:double ;\n" +
							"        itsrdf:taClassRef     dbo:Location ;\n" +
							"        itsrdf:taIdentRef     <http://dbpedia.org/resource/Brandenburg> .\n" +
							"\n" +
							"<http://dkt.dfki.de/documents/#char=31,34>\n" +
							"        a                     nif:String , nif:RFC5147String ;\n" +
							"        nif:anchorOf          \"She\"^^xsd:string ;\n" +
							"        nif:beginIndex        \"31\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:endIndex          \"34\"^^xsd:nonNegativeInteger ;\n" +
							"        nif:referenceContext  <http://dkt.dfki.de/documents/#char=0,86> ;\n" +
							"        owl:sameAs            \"http://dkt.dfki.de/documents/#char=0,13\"^^xsd:string .\n" +
							"";
		
				NifDocument doc = new NifDocument();
				nifModel = doc.unserializeRDF(nifString, "text/turtle").getModel();
				nifModel = resolveCoreferencesNIF(nifModel);
	 }
	

	 
	 static String readFile(String path, Charset encoding) 
			  throws IOException 
			{
			  byte[] encoded = Files.readAllBytes(Paths.get(path));
			  return new String(encoded, encoding);
			}

	public static Model resolveCoreferencesNIF(Model nifModel) {
		
		String nifString = NIFReader.extractIsString(nifModel);
		Properties props = new Properties();

		props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,coref");
	    props.setProperty("coref.algorithm", "neural");
	    //CoreDocument document = new CoreDocument(nifString);
	    Annotation document = new Annotation(nifString);   
	    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	    pipeline.annotate(document);
	    //Map<Integer, CorefChain> corefChains = document.corefChains();
	    //System.out.println("DEBUGGING COREFCHAINSMAP:"+corefChains);
	    List<String[]> ents = NIFReader.extractEntityIndices(nifModel);
	    if (ents == null){
	    	return nifModel; // in this case we do not want to do anything, just return the current model
	    }
	    
	 // converting this to hashmap structure with indices as key, so we don't have to loop through it everytime for every coreference mention, but can check if it is in the hash right away
	    HashMap<String, String> entIndexMap = new HashMap<String, String>();
	    for (String[] sa : ents){
	    	entIndexMap.put(sa[3] + "_" + sa[4], sa[1]);
	    }
	
	    // looping through all chains
	    for (CorefChain cc : document.get(CorefCoreAnnotations.CorefChainAnnotation.class).values()){
	    	// for individual chain, first check if there is a match with an existing entity
	    	System.out.println("CC:"+cc);
	   		List<Integer> match = new ArrayList<Integer>();
			
		   	for (IntPair key : cc.getMentionMap().keySet()){
		   		Set<CorefMention> set = cc.getMentionMap().get(key);
		   		String existingIndex = null;
		   		for (CorefMention cm : set){
		   			System.out.println("CM:"+cm.mentionSpan);
		   			List<CoreLabel> tokens = document.get(CoreAnnotations.SentencesAnnotation.class).get(cm.sentNum-1).get(CoreAnnotations.TokensAnnotation.class);
		   			int startIndex = tokens.get(cm.startIndex-1).beginPosition();
		   			int endIndex = startIndex + cm.mentionSpan.length();
		   			for (String[] sa : ents) { // PB 28.10.2019: because there is a high likelihood
		   				// that coreNLP will not recognise the exact same entity spans as our NER module (but larger/smaller text spans), loosening up the matching here
		   				// such that it also matches/establishes a link if both text spans overlap (may want to play around with the size of the overlap if this proves too lenient)
		   				if (Integer.parseInt(sa[3]) >= startIndex && Integer.parseInt(sa[4]) <= endIndex) {
		   					match.add(Integer.parseInt(sa[3]));
		   					match.add(Integer.parseInt(sa[4]));
		   					// not sure what happens (if it can happen at all) when we have multiple matches. Try out with large text to see!
		   				}
		   			}
		   		}
		   	}
			System.out.println("MATCH:"+match);
			// if there is a match, loop through this chain again, and link every mention that is not the orig entity itself to the orig entity
			for (IntPair key : cc.getMentionMap().keySet()) {
				Set<CorefMention> set = cc.getMentionMap().get(key);
				for (CorefMention cm : set){
					List<CoreLabel> tokens = document.get(CoreAnnotations.SentencesAnnotation.class).get(cm.sentNum-1).get(CoreAnnotations.TokensAnnotation.class);
		   			int startIndex = tokens.get(cm.startIndex-1).beginPosition();
		   			int endIndex = startIndex + cm.mentionSpan.length();
		   			if (match.get(0) == startIndex && endIndex == match.get(1)) {
		   				//pass, it's the original entity
		   			}
		   			else {
		   				String existingEntityURI = NIFReader.extractDocumentURI(nifModel) + "#char=" + match.get(0).toString() + "," + match.get(1).toString();
    					System.out.println("Orig ent:"+existingEntityURI);
    					String existingTaIdentRef = NIFReader.extractTaIdentRefWithEntityURI(nifModel, existingEntityURI);
    					addCoreferenceAnnotation(nifModel, startIndex, endIndex, cm.mentionSpan, existingEntityURI, existingTaIdentRef);
    					System.out.println("referrer:" + cm.mentionSpan);
    					System.out.println("referree:" + nifString.substring(match.get(0), match.get(1)));
    					System.out.println("in sentence:" + document.get(CoreAnnotations.SentencesAnnotation.class).get(cm.sentNum-1));
    					System.out.println("\n");
		   			}
				}
			}
		   	
	    }
	    	
		return nifModel;
	}
	 
	public static Model resolveCoreferencesNIF_old(Model nifModel) { // migrated from 3.6.0 to 3.9.2 in oct/nov 2019. The code below is for the 3.6.0 version
		
		 String nifString = NIFReader.extractIsString(nifModel);
		 //System.out.println("DEBUGGING input text:"+nifString);
		 //Put separation of string here
		 //Also store the indexes from where to where the separation goes
		 //we have to make sure, that only entities from the current batch are considered
//		 List<String> strings = new ArrayList<String>();
//		 int index = 0;
//		 while (index < nifString.length()) {
//		     strings.add(nifString.substring(index, Math.min(index + 50000,nifString.length())));
//		     index += 50000;
//		 }
		 Annotation document = new Annotation(nifString);   
		 Properties props = new Properties();
		 //props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,mention,dcoref");
		 props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,coref");
		 //props.setProperty("coref.algorithm", "neural");		// props.setProperty("coref.doClustering", "false");
		// props.setProperty("coref.md.type", "hybrid");
		//props.setProperty("dcoref.maxdist", "3");
		props.setProperty("dcoref.maxdist", "9");
		props.setProperty("parse.maxlen", "70");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		pipeline.annotate(document);

		    List<String[]> ents = NIFReader.extractEntityIndices(nifModel);
		    if (ents == null){
		    	return nifModel; // in this case we do not want to do anything, just return the current model
		    }
			   /*
			    * 0:ITSRDF.taClassRef
			    * 1:anchorOf
			    * 2:taIdentRef
			    * 3:beginIndex
			    * 4:endIndex
			    */
		    // converting this to hashmap structure with indices as key, so we don't have to loop through it everytime for every coreference mention, but can check if it is in the hash right away
		    HashMap<String, String> entIndexMap = new HashMap<String, String>();
		    for (String[] sa : ents){
		    	entIndexMap.put(sa[3] + "_" + sa[4], sa[1]);
		    }
		    
		    for (CorefChain cc : document.get(CorefCoreAnnotations.CorefChainAnnotation.class).values()){
		    	for (IntPair key : cc.getMentionMap().keySet()){
		    		Set<CorefMention> set = cc.getMentionMap().get(key);
		    		if (set.size() > 1){ // we only want chains with more than one element, otherwise there is nothing to refer to
		    			String existingIndex = null;
		    			for (CorefMention cm : set){ // first pass is to check if there is a recognized entity in this coref chain
		    				List<CoreLabel> tokens = document.get(CoreAnnotations.SentencesAnnotation.class).get(cm.sentNum-1).get(CoreAnnotations.TokensAnnotation.class);
		    				int startIndex = tokens.get(cm.startIndex-1).beginPosition();
		    				int endIndex = startIndex + cm.mentionSpan.length();
		    				if (entIndexMap.containsKey(startIndex + "_" + endIndex)){
		    					existingIndex = startIndex + "_" + endIndex;
		    				}
		    			}
		    			if (existingIndex != null){ // second pass; if there was a matching entity, link them
		    				for (CorefMention cm : set){
		    					List<CoreLabel> tokens = document.get(CoreAnnotations.SentencesAnnotation.class).get(cm.sentNum-1).get(CoreAnnotations.TokensAnnotation.class);
			    				int startIndex = tokens.get(cm.startIndex-1).beginPosition();
			    				int endIndex = startIndex + cm.mentionSpan.length();
			    				if (entIndexMap.containsKey(startIndex + "_" + endIndex)){
			    					// skip, this one is in the nif already
			    				}
			    				else{
			    					String existingEntityURI = NIFReader.extractDocumentURI(nifModel) + "#char=" + existingIndex.split("_")[0] + "," + existingIndex.split("_")[1]; // TODO: fix this. This will probably outdated again once we move to NIF 2.1 and is quite a hacky way of retrieving an entity URI. Should be a neat way of getting that, so use that!
			    					String existingTaIdentRef = NIFReader.extractTaIdentRefWithEntityURI(nifModel, existingEntityURI);
			    					addCoreferenceAnnotation(nifModel, startIndex, endIndex, cm.mentionSpan, existingEntityURI, existingTaIdentRef);
			    					System.out.println("referrer:" + cm.mentionSpan);
			    					System.out.println("referree:" + nifString.substring(Integer.parseInt(existingIndex.split("_")[0]), Integer.parseInt(existingIndex.split("_")[1])));
			    					System.out.println("in sentence:" + document.get(CoreAnnotations.SentencesAnnotation.class).get(cm.sentNum-1));
			    					System.out.println("\n");
			    					
			    		            //System.out.println("in sentence:" + document.get(CoreAnnotations.SentencesAnnotation.class).get(cm.sentNum-1));
//			    		            String temp = "";
//			    		            for (int j  = Integer.max(0, cm.sentNum-1 -3) ; j < Integer.min(document.get(CoreAnnotations.SentencesAnnotation.class).size(), cm.sentNum-1 + 3); j++){
//			    		             temp += document.get(CoreAnnotations.SentencesAnnotation.class).get(j) + "\n";
//			    		            }
			    		            
//			    		            System.out.println("somewhere in the context of...:" + temp);
//			    		            System.out.println("\n");
			    				}
		    				}
		    			}
		    		}
		    		
		    	}
		    	
		    }
		    
		return nifModel;
	}

	public static void addCoreferenceAnnotation(Model outModel, int startIndex, int endIndex, String text, String sameAsEntityURI, String sameAsEntityTaIdentRef){
		
		String docURI = NIFReader.extractDocumentURI(outModel);
		String spanUri = new StringBuilder().append(docURI).append("#char=").append(startIndex).append(',').append(endIndex).toString();

		Map<String, String> prefixes = outModel.getNsPrefixMap();
		if (!prefixes.containsKey("owl")){
			prefixes.put("owl", "http://www.w3.org/2002/07/owl#");
			outModel.setNsPrefixes(prefixes);
		}
		
		Resource spanAsResource = outModel.createResource(spanUri);
		outModel.add(spanAsResource, RDF.type, NIF.String);
		outModel.add(spanAsResource, RDF.type, NIF.RFC5147String);
		outModel.add(spanAsResource, NIF.anchorOf, outModel.createTypedLiteral(text, XSDDatatype.XSDstring));
		outModel.add(spanAsResource, NIF.beginIndex, outModel.createTypedLiteral(startIndex, XSDDatatype.XSDnonNegativeInteger));
		outModel.add(spanAsResource, NIF.endIndex, outModel.createTypedLiteral(endIndex, XSDDatatype.XSDnonNegativeInteger));
		outModel.add(spanAsResource, NIF.referenceContext, outModel.createResource(NIFReader.extractDocumentWholeURI(outModel)));
		outModel.add(spanAsResource, OWL.sameAs, outModel.createTypedLiteral(sameAsEntityURI, XSDDatatype.XSDstring));
		if (sameAsEntityTaIdentRef != null){
			outModel.add(spanAsResource, ITSRDF.taIdentRef, outModel.createTypedLiteral(sameAsEntityTaIdentRef, XSDDatatype.XSDstring));
		}
		
	}

}
