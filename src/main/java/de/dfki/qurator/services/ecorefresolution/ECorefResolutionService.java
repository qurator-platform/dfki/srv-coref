package de.dfki.qurator.services.ecorefresolution;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

import javax.annotation.PostConstruct;

import org.apache.jena.rdf.model.Model;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import de.dfki.qurator.nif.NifDocument;
import de.dfki.qurator.nif.processing.JenaRDFConversionService;
import de.dfki.qurator.services.ecorefresolution.modules.Corefinizer;
import de.dfki.qurator.services.ecorefresolution.persistence.Analysis;
import de.dfki.qurator.services.ecorefresolution.persistence.AnalysisRepository;

/**
 * @author Julian Moreno Schneider julian.moreno_schneider@dfki.de
 *
 * The whole documentation about openNLP examples can be found in https://opennlp.apache.org/documentation/1.6.0/manual/opennlp.html
 *
 */

@Component
public class ECorefResolutionService {
    
	Logger logger = Logger.getLogger(ECorefResolutionService.class);

	@Autowired
	AnalysisRepository analysisRepository;
	
	HashMap<String, Analysis> analysiss = new HashMap<String, Analysis>();
	
	public ECorefResolutionService() {
	
	}
	
	@PostConstruct
	public void initializeModels(){
		/**
		 * TIMER not needed currently, implemented for future usage.
		 */
//		Timer timerObj = new Timer(true);
//		timerObj.scheduleAtFixedRate(new TimerTask() {
//			@Override
//			public void run() {
//				System.out.println("Updating the CRUD.");
//				// TODO Auto-generated method stub
//			}
//		}, 0, 5000);
	}
	
	public String analyzeSynchronous(String textForProcessing, String informat, String outformat, String prefix, String mode, String languageParam, boolean isContent, String outputCallback) throws Exception, Exception,IOException, Exception {
		try {
    		Model nifModel; 
    		NifDocument d = new NifDocument();
       		nifModel = d.unserializeRDF(textForProcessing, informat).getModel();
    		if(nifModel==null) {
    			throw new Exception("The NIFMODEL is null at analyzeSynchronous in CorefService.");
    		}
    		nifModel = analyze(nifModel, languageParam);
    		return d.serialize(outformat);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

	}

	public String analyzeAsynchronous(String textForProcessing, String informat, String outformat, String prefix, String mode, String languageParam, boolean isContent, String outputCallback) throws Exception, Exception,IOException, Exception {
		try {
			String analysisId = generateAnalysisId();
			Analysis a = new Analysis();
			a.setAnalysisId(analysisId);
			a.setInput(textForProcessing);
			a.setInputFormat(informat);
			a.setOutputFormat(outformat);
			analysisRepository.save(a);
			analysiss.put(analysisId, a);
			CompletableFuture<Void> contentFuture = CompletableFuture.supplyAsync(() -> {
				String s = "";
				try {
//					System.out.println("TEXT FOR PROCESSING: "+textForProcessing);
		    		Model nifModel; 
		    		NifDocument d = new NifDocument();
		       		nifModel = d.unserializeRDF(textForProcessing, informat).getModel();
		    		if(nifModel==null) {
		    			throw new Exception("The NIFMODEL is null at analyzeSynchronous in CorefService.");
		    		}
		    		nifModel = analyze(nifModel, languageParam);
					Model nifModel2 = nifModel;
					NifDocument d2 = new NifDocument(nifModel2);
		    		return d2.serialize(outformat);
				} catch (Exception e) {
					e.printStackTrace();
					s = "EXCEPTION";
				}
				return s;
			}).thenAccept(t -> {
				processOutput(analysisId, textForProcessing, informat, t, outformat, outputCallback);
			});
			return analysisId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public void processOutput(String analysisId, String input, String informat, String output, String outformat,
			String outputCallback) {
		try {
			Analysis a2 = analysiss.get(analysisId);
			a2.setAnalysisId(analysisId);
			a2.setInput(input);
			a2.setInputFormat(informat);
			a2.setOutput(output);
			a2.setOutputFormat(outformat);
			a2.setFinished();
//			System.out.println("saving new analysis: "+a2.getAnalysisId()+" with status: "+a2.getStatus2());
//			analysisRepository.save(a2);
			analysiss.put(analysisId,a2);
//			System.out.println("Status2 after updating:"+a3.getStatus2());
			if(outputCallback!=null && !outputCallback.equalsIgnoreCase("")) {
				//							System.out.println("OUTPUTCALLBACK: "+outputCallback);
				//							System.out.println("OUTPUTCALLBACK BODY: "+s);
				HttpResponse<String> callbackResponse = Unirest.post(outputCallback).body(output).asString();
				if(callbackResponse.getStatus()!=200) {
					throw new Exception("Error reporting outputCallback from Microservice Geolocation for Analysis [" + analysisId + "] with ERROR: " + callbackResponse.getStatus() + " ["+callbackResponse.getBody()+"]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String generateAnalysisId() {
		Date d = new Date();
		return d.getTime()+"";
	}
	
	public String checkStatus(String analysisId) throws Exception {
		Analysis a = analysisRepository.findOneByAnalysisId(analysisId);
		if(a==null) {
			throw new Exception("The provided 'analysisId' does not match any existing Analysis.");
		}
		return a.getStatus().toString();
	}

	public String getOutput(String analysisId, String contentTypeHeader) throws Exception {
		boolean done = false;
		Analysis a = null;
//		List<Analysis> analyses = analysisRepository.findAll();
//		System.out.println("AVAILABLE ANALYSIS:");
//		for (Analysis analysis : analyses) {
//			System.out.println(analysis.getAnalysisId());
//		}
		while(!done) {
//			a = analysisDAO.findOneByAnalysisId(analysisId);
			a = analysiss.get(analysisId);
			if(a==null) {
//				System.out.println("Sleep because is NULL...");
				Thread.sleep(1000);
//				throw new Exception("The provided 'analysisId' does not match any existing Analysis.");
			}
			else {
				if(a.getStatus2().equalsIgnoreCase("FINISHED")) {
					done=true;
				}
				else {
//					System.out.println(a.getStatus2());
//					System.out.println("Sleep because is NOT FINISHED...");
					Thread.sleep(1000);
				}
			}
		}
		String output = a.getOutput();
		if(a.getOutputFormat().equalsIgnoreCase(contentTypeHeader)) {
			return output;
		}
		else {
//			System.out.println(contentTypeHeader);
//			System.out.println(a.getOutputFormat());
//			System.out.println(output);
			
			NifDocument d = new NifDocument();
			
			Model model = d.unserializeRDF(output, a.getOutputFormat()).getModel();
			NifDocument d2 = new NifDocument(model);
			return d2.serialize(contentTypeHeader);
			
		}
	}

	public Model analyze(Model nifModel, String languageParam) throws Exception, Exception,IOException, Exception {
        try {
    		if (languageParam.equals("en")){
    		}
    		else{
    			logger.error("Unsupported language ["+languageParam+"] ");
    			throw new Exception("Unsupported language ["+languageParam+"] ");
    		}

    		nifModel = Corefinizer.resolveCoreferencesNIF(nifModel);
			return nifModel;
        } catch (Exception e) {
        	logger.error(e.getMessage());
            throw e;
    	}
	}

}
